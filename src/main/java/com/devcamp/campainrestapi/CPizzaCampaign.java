package com.devcamp.campainrestapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class CPizzaCampaign {
    @CrossOrigin
    @GetMapping("/devcamp-simple")
    public String simple() {
        return "testt campaign";
    }

    @GetMapping("/devcamp-date")
    public String getDateViet() {
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        return String.format("Hello pizza Loverrr!! Hôm nay %s mua 1 tặng 1 mại dô",
                dtfVietNam.format(today));
    }

}
