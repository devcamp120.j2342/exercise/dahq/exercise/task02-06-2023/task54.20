package com.devcamp.campainrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampainrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CampainrestapiApplication.class, args);
	}

}
